package ru.tecomgroup.snmp_agent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import ru.tecomgroup.snmp_agent.model.type_enum.IfAdminStatus;
import ru.tecomgroup.snmp_agent.model.type_enum.IfOperStatus;
import ru.tecomgroup.snmp_agent.model.type_enum.IfType;

@Entity
@Table(name = "tem_interface")
public class TEM_Interface extends TEM_Equipment {

	private static final long serialVersionUID = 3290359501901408432L;

	@Column
	private Integer ifIndex;

	@Column
	private String ifDescr;

	@Enumerated(EnumType.STRING)
	@Column(length = 50)
	private IfType ifType;

	@Column
	private Integer ifMtu;

	@Column
	private Integer ifSpeed;

	@Column
	private String ifPhysAddress;

	@Enumerated(EnumType.STRING)
	@Column(length = 8)
	private IfAdminStatus ifAdminStatus;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private IfOperStatus ifOperStatus;

	public Integer getIfIndex() {
		return ifIndex;
	}

	public void setIfIndex(Integer ifIndex) {
		this.ifIndex = ifIndex;
	}

	public String getIfDescr() {
		return ifDescr;
	}

	public void setIfDescr(String ifDescr) {
		this.ifDescr = ifDescr;
	}

	public IfType getIfType() {
		return ifType;
	}

	public void setIfType(IfType ifType) {
		this.ifType = ifType;
	}

	public Integer getIfMtu() {
		return ifMtu;
	}

	public void setIfMtu(Integer ifMtu) {
		this.ifMtu = ifMtu;
	}

	public Integer getIfSpeed() {
		return ifSpeed;
	}

	public void setIfSpeed(Integer ifSpeed) {
		this.ifSpeed = ifSpeed;
	}

	public String getIfPhysAddress() {
		return ifPhysAddress;
	}

	public void setIfPhysAddress(String ifPhysAddress) {
		this.ifPhysAddress = ifPhysAddress;
	}

	public IfAdminStatus getIfAdminStatus() {
		return ifAdminStatus;
	}

	public void setIfAdminStatus(IfAdminStatus ifAdminStatus) {
		this.ifAdminStatus = ifAdminStatus;
	}

	public IfOperStatus getIfOperStatus() {
		return ifOperStatus;
	}

	public void setIfOperStatus(IfOperStatus ifOperStatus) {
		this.ifOperStatus = ifOperStatus;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(TEM_Interface.class.getName()).append("[");
		result.append("ifIndex=").append(ifIndex).append(", ");
		result.append("ifDescr=").append(ifDescr).append(", ");
		result.append("ifType=").append(ifType).append(", ");
		result.append("ifMtu=").append(ifMtu).append(", ");
		result.append("ifSpeed=").append(ifSpeed).append(", ");
		result.append("ifPhysAddress=").append(ifPhysAddress).append(", ");
		result.append("ifAdminStatus=").append(ifAdminStatus).append(", ");
		result.append("ifOperStatus=").append(ifOperStatus).append("]");
		return result.toString();
	}
	
}
