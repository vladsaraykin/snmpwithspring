package ru.tecomgroup.snmp_agent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tem_system")
public class TEM_System extends TEM_BaseEntity {

	private static final long serialVersionUID = 6656843978173558410L;
	
	@Column
	private String sysDescr;
	
	@Column
	private String sysObjectID;
	
	@Column
	private String sysUpTime;
	
	@Column
	private String sysContact;
	
	@Column
	private String sysName;
	
	@Column
	private String sysLocation;
	
	@Column
	private String sysServices;
	
	public String getSysDescr() {
		return sysDescr;
	}

	public void setSysDescr(String sysDescr) {
		this.sysDescr = sysDescr;
	}

	public String getSysObjectID() {
		return sysObjectID;
	}

	public void setSysObjectID(String sysObjectID) {
		this.sysObjectID = sysObjectID;
	}

	public String getSysUpTime() {
		return sysUpTime;
	}

	public void setSysUpTime(String sysUpTime) {
		this.sysUpTime = sysUpTime;
	}

	public String getSysContact() {
		return sysContact;
	}

	public void setSysContact(String sysContact) {
		this.sysContact = sysContact;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getSysLocation() {
		return sysLocation;
	}

	public void setSysLocation(String sysLocation) {
		this.sysLocation = sysLocation;
	}

	public String getSysServices() {
		return sysServices;
	}

	public void setSysServices(String sysServices) {
		this.sysServices = sysServices;
	}
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(TEM_System.class.getName()).append("[");
		result.append("sysDescr=").append(sysDescr).append(", ");
		result.append("sysObjectID=").append(sysObjectID).append(", ");
		result.append("sysContact=").append(sysContact).append(", ");
		result.append("sysName=").append(sysName).append(", ");
		result.append("sysLocation=").append(sysLocation).append(", ");
		result.append("sysServices=").append(sysServices).append(", ");
		result.append("sysContact=").append(sysContact).append("]");
		return result.toString();
	}
}
