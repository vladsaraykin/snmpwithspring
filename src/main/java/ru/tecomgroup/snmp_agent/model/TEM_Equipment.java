package ru.tecomgroup.snmp_agent.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class TEM_Equipment extends TEM_BaseEntity {

	private static final long serialVersionUID = 7686895648570970142L;

	@Column
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
