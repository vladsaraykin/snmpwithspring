package ru.tecomgroup.snmp_agent.model.type_enum;

public enum IfAdminStatus {
	up(1), down(2), testing(3);
	private final int value;

	private IfAdminStatus(int value) {
		this.value = value;
	}

	public static IfAdminStatus getIfAdminStatusById(int value) {
		for (IfAdminStatus state : IfAdminStatus.values()) {
			if (state.value == value) {
				return state;
			}
		}
		return null;
	}
}
