package ru.tecomgroup.snmp_agent.model.type_enum;

public enum IfOperStatus {
	up(1), down(2), testing(3), unknown(4), dormant(5), notPresent(6), lowerLayerDown(7);

	private final int value;

	private IfOperStatus(int value) {
		this.value = value;
	}

	public static IfOperStatus getIfOperStatus(int value) {
		for (IfOperStatus state : IfOperStatus.values()) {
			if (state.value == value) {
				return state;
			}
		}
		return null;
	}
}
