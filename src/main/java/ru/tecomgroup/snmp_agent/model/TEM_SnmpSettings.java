package ru.tecomgroup.snmp_agent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tem_device_settings")
public class TEM_SnmpSettings extends TEM_BaseEntity {

	private static final long serialVersionUID = 2158783817109926067L;
	
	@Column
	private String address;
	
	@Column(name = "read_community")
	private String readCommunity;
	
	@Column(name = "write_community")
	private String writeCommunity;
	
	@Column(name = "request_port")
	private Integer requestPort;
		
	@Column(name = "snmp_version")
	private Integer snmpVersion;
	
	@Column
	private Integer retries;
	
	@Column
	private Long timeout;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReadCommunity() {
		return readCommunity;
	}

	public void setReadCommunity(String readCommunity) {
		this.readCommunity = readCommunity;
	}

	public String getWriteCommunity() {
		return writeCommunity;
	}

	public void setWriteCommunity(String writeCommunity) {
		this.writeCommunity = writeCommunity;
	}

	public Integer getRequestPort() {
		return requestPort;
	}

	public void setRequestPort(Integer requestPort) {
		this.requestPort = requestPort;
	}

	public Integer getSnmpVersion() {
		return snmpVersion;
	}

	public void setSnmpVersion(Integer snmpVersion) {
		this.snmpVersion = snmpVersion;
	}

	public Integer getRetries() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	public Long getTimeout() {
		return timeout;
	}

	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(TEM_SnmpSettings.class.getName()).append("[");
		result.append("address=").append(address).append(", ");
		result.append("requestPort=").append(requestPort).append(", ");
		result.append("readCommunity=").append(readCommunity).append(", ");
		result.append("writeCommunity=").append(writeCommunity).append(", ");
		result.append("snmpVersion=").append(snmpVersion).append(", ");
		result.append("retries=").append(retries).append(", ");
		result.append("timeout=").append(timeout).append("]");
		return result.toString();
	}
		
}
