package ru.tecomgroup.snmp_agent.model;


import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tem_device")
public class TEM_Device extends TEM_Equipment {

	private static final long serialVersionUID = 5401839062355278841L;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "tem_system_id")
	private TEM_System system = new TEM_System();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "tem_snmpsetting_id", nullable = false)
	private TEM_SnmpSettings setting;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "tem_device_id")
	@MapKey(name = "ifIndex")
	private Map<Integer, TEM_Interface> interfaces = new HashMap<>();

	public TEM_System getSystem() {
		return system;
	}

	public void setSystem(TEM_System system) {
		this.system = system;
	}

	public TEM_SnmpSettings getSetting() {
		return setting;
	}

	public void setSetting(TEM_SnmpSettings setting) {
		this.setting = setting;
	}

	public Map<Integer, TEM_Interface> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(Map<Integer, TEM_Interface> interfaces) {
		this.interfaces = interfaces;
	}

	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(TEM_Device.class.getName()).append("[");
		result.append("Id=").append(getId()).append(", ");
		result.append("name=").append(getName()).append(", ");
		result.append("system=").append(system).append(", ");
		result.append("setting=").append(setting).append("]");
		return result.toString();
	}
}
