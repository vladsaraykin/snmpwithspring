package ru.tecomgroup.snmp_agent;

import java.util.Scanner;

public class ConsoleScanner {
	private Scanner scanner = new Scanner(System.in);

	public ConsoleScanner() {
		super();
	}

	private Integer nextInt() {
		Integer result;
		String resultStr = scanner.nextLine();
		if(resultStr.isEmpty()) {
			return null;
		}
//		TODO
		try {
		result = Integer.parseInt(resultStr);
		}catch(Exception e) {
			while (true) {
				System.out.print("Invalid value entered. Please, try again: ");
				Integer correctValue = nextInt();
				if (correctValue != null) {
					result = correctValue;
					break;
				}
			}
		}
		return result;
	}

	private String nextString() {
		String result = scanner.nextLine();
		if(result.isEmpty()){
			return null;
		}
		return result;
	}

	public Integer getIntegerValue() {
		return getIntegerValue(null);
	}

	public Integer getIntegerValue(String title) {
		return getIntegerValue(title, null);
	}

	public Integer getIntegerValue(String title, Integer defaultValue) {
		if (title != null) {
			System.out.print(defaultValue != null ? title + "(default is " + defaultValue + ")" : title);
		}
		Integer result = nextInt();
		if (result == null) {
			if (defaultValue == null) {
				while (true) {
					System.out.print("Invalid value entered. Please, try again: ");
					Integer correctValue = nextInt();
					if (correctValue != null) {
						result = correctValue;
						break;
					}
				}
			} else {
				result = defaultValue;
			}
		}
		return result;
	}

	public String getStringValue() {
		return getStringValue(null);
	}

	public String getStringValue(String title) {
		return getStringValue(title, null);
	}

	public String getStringValue(String title, String defaultValue) {
		
		if (title != null) {
			System.out.print(defaultValue != null ? title + "(default is '" + defaultValue + "')" : title);
		}
		String result = nextString();
		if (result == null) {
			if (defaultValue == null) {
				while (true) {
					System.out.print("Invalid value entered. Please, try again: ");
					String correctValue = nextString();
					if (correctValue != null) {
						result = correctValue;
						break;
					}
				}
			} else {
				result = defaultValue;
			}
		}
		return result;
	}
}
