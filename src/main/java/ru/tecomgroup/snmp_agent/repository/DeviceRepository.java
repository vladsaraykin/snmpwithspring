package ru.tecomgroup.snmp_agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tecomgroup.snmp_agent.model.TEM_Device;

public interface DeviceRepository extends JpaRepository<TEM_Device, Long> {

}
