package ru.tecomgroup.snmp_agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tecomgroup.snmp_agent.model.TEM_System;

public interface TEMSystemRepository extends JpaRepository<TEM_System, Long> {

}
