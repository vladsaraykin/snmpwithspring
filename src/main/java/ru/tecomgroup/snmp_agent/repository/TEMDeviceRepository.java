package ru.tecomgroup.snmp_agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tecomgroup.snmp_agent.model.TEM_Device;

public interface TEMDeviceRepository extends JpaRepository<TEM_Device, Long> {

//	@Query("select d from TEM_Device d WHERE d.localId = :localId")
//	TEM_Device getDeviceByLocalId(@Param("localId") Long localId);

}
