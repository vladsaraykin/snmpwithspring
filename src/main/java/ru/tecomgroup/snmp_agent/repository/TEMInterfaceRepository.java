package ru.tecomgroup.snmp_agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tecomgroup.snmp_agent.model.TEM_Interface;

public interface TEMInterfaceRepository extends JpaRepository<TEM_Interface, Long> {

}
