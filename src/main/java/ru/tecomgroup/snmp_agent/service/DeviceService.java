package ru.tecomgroup.snmp_agent.service;

import ru.tecomgroup.snmp_agent.model.TEM_Device;
import java.util.List;

public interface DeviceService {
	TEM_Device addDevice(TEM_Device device);

	void deleteDeviceById(Long id);

	TEM_Device editDevice(TEM_Device device);

	List<TEM_Device> getAll();
	
	TEM_Device getDeviceById(Long id);
}
