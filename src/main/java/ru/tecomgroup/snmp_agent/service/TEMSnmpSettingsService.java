package ru.tecomgroup.snmp_agent.service;

import ru.tecomgroup.snmp_agent.model.TEM_SnmpSettings;
import java.util.List;

public interface TEMSnmpSettingsService {
	TEM_SnmpSettings addTemDevice(TEM_SnmpSettings setting);

	void deleteTemSnmpSettings(long id);

	TEM_SnmpSettings getTemSnmpSettingsById(long id);

	TEM_SnmpSettings editTemSnmpSettings(TEM_SnmpSettings settings);

	List<TEM_SnmpSettings> getAll();
}
