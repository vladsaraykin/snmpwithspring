package ru.tecomgroup.snmp_agent.service;

import ru.tecomgroup.snmp_agent.model.TEM_Device;
import java.util.List;

public interface TemDeviceService {
	TEM_Device addTemDevice(TEM_Device device);

	void deleteTemDeviceById(Long id);

	TEM_Device editTemDevice(TEM_Device device);

	List<TEM_Device> getAll();
	
	TEM_Device getTemDeviceById(Long id);
}
