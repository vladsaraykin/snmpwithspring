package ru.tecomgroup.snmp_agent.service;

import ru.tecomgroup.snmp_agent.model.TEM_System;
import java.util.List;

public interface TEMSystemService {
	TEM_System addTemSystem(TEM_System system);

	void deleteTemSystem(long id);

	TEM_System getTemSystemById(long id);

	TEM_System editTemSystem(TEM_System system);

	List<TEM_System> getAll();
}
