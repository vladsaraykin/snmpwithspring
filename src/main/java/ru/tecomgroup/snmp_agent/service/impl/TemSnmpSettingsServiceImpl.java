package ru.tecomgroup.snmp_agent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tecomgroup.snmp_agent.model.TEM_SnmpSettings;
import ru.tecomgroup.snmp_agent.repository.TEMSnmpSettingsRepository;
import ru.tecomgroup.snmp_agent.service.TEMSnmpSettingsService;

import java.util.List;

@Service
public class TemSnmpSettingsServiceImpl implements TEMSnmpSettingsService {
	@Autowired
	private TEMSnmpSettingsRepository snmpSettingsRepository;

	@Override
	public TEM_SnmpSettings addTemDevice(TEM_SnmpSettings setting) {
		return snmpSettingsRepository.saveAndFlush(setting);
	}

	@Override
	public void deleteTemSnmpSettings(long id) {
		snmpSettingsRepository.deleteById(id);
	}

	@Override
	public TEM_SnmpSettings getTemSnmpSettingsById(long localId) {
		return null;
	}

	@Override
	public TEM_SnmpSettings editTemSnmpSettings(TEM_SnmpSettings settings) {
		return snmpSettingsRepository.saveAndFlush(settings);
	}

	@Override
	public List<TEM_SnmpSettings> getAll() {
		return snmpSettingsRepository.findAll();
	}
}
