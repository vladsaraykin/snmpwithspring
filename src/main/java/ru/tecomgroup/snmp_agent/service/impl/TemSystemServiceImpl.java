package ru.tecomgroup.snmp_agent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tecomgroup.snmp_agent.model.TEM_System;
import ru.tecomgroup.snmp_agent.repository.TEMSystemRepository;
import ru.tecomgroup.snmp_agent.service.TEMSystemService;

import java.util.List;

@Service
public class TemSystemServiceImpl implements TEMSystemService {
	@Autowired
	private TEMSystemRepository systemRepository;

	@Override
	public TEM_System addTemSystem(TEM_System system) {
		return systemRepository.saveAndFlush(system);
	}

	@Override
	public void deleteTemSystem(long id) {
		systemRepository.deleteById(id);
	}

	@Override
	public TEM_System getTemSystemById(long localId) {
		return null;
	}

	@Override
	public TEM_System editTemSystem(TEM_System system) {
		return systemRepository.saveAndFlush(system);
	}

	@Override
	public List<TEM_System> getAll() {
		return systemRepository.findAll();
	}
}
