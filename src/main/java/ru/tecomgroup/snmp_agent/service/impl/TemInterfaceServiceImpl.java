package ru.tecomgroup.snmp_agent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tecomgroup.snmp_agent.model.TEM_Interface;
import ru.tecomgroup.snmp_agent.repository.TEMInterfaceRepository;
import ru.tecomgroup.snmp_agent.service.TEMInterfaceService;

import java.util.List;
@Service
public class TemInterfaceServiceImpl implements TEMInterfaceService {
    @Autowired
    private TEMInterfaceRepository temInterfaceRepository;

    @Override
    public TEM_Interface addTemInterface(TEM_Interface temInterface) {
        return temInterfaceRepository.saveAndFlush(temInterface);
    }

    @Override
    public void deleteTemInterface(long id) {
        temInterfaceRepository.deleteById(id);
    }

    @Override
    public TEM_Interface getTemInterfaceById(long id) {
        return null;
    }

    @Override
    public TEM_Interface editTemInterface(TEM_Interface temInterface) {
        return temInterfaceRepository.saveAndFlush(temInterface);
    }

    @Override
    public List<TEM_Interface> getAll() {
        return temInterfaceRepository.findAll();
    }
}
