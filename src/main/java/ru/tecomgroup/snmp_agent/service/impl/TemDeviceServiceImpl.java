package ru.tecomgroup.snmp_agent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tecomgroup.snmp_agent.model.TEM_Device;
import ru.tecomgroup.snmp_agent.repository.TEMDeviceRepository;
import ru.tecomgroup.snmp_agent.service.TemDeviceService;

import java.util.List;

@Service
public class TemDeviceServiceImpl implements TemDeviceService {
	@Autowired
	private TEMDeviceRepository temDeviceRepository;

	public void setTemDeviceRepository(TEMDeviceRepository temDeviceRepository) {
		this.temDeviceRepository = temDeviceRepository;
	}

	@Override
	public TEM_Device addTemDevice(TEM_Device device) {
		return temDeviceRepository.saveAndFlush(device);
	}

	@Override
	public TEM_Device editTemDevice(TEM_Device device) {

		return temDeviceRepository.saveAndFlush(device);
	}

	@Override
	public List<TEM_Device> getAll() {
		return temDeviceRepository.findAll();
	}

	@Override
	public TEM_Device getTemDeviceById(Long id) {
		return temDeviceRepository.getOne(id);
	}

	@Override
	public void deleteTemDeviceById(Long id) {
		temDeviceRepository.deleteById(id);
	}

}
