package ru.tecomgroup.snmp_agent.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tecomgroup.snmp_agent.model.TEM_Device;
import ru.tecomgroup.snmp_agent.repository.DeviceRepository;
import ru.tecomgroup.snmp_agent.service.DeviceService;

import java.util.List;

@Service
public class DeviceServiceImpl implements DeviceService {
	@Autowired
	private DeviceRepository deviceRepository;

	@Override
	public TEM_Device addDevice(TEM_Device device) {
		return deviceRepository.saveAndFlush(device);
	}

	@Override
	public TEM_Device editDevice(TEM_Device device) {
		return deviceRepository.saveAndFlush(device);
	}

	@Override
	public List<TEM_Device> getAll() {
		return deviceRepository.findAll();
	}

	@Override
	public TEM_Device getDeviceById(Long id) {
		TEM_Device device = deviceRepository.findById(id).get();
		return device;
	}

	@Override
	public void deleteDeviceById(Long id) {
		deviceRepository.deleteById(id);
	}

}
