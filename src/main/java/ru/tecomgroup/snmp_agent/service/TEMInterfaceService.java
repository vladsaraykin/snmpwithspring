package ru.tecomgroup.snmp_agent.service;

import ru.tecomgroup.snmp_agent.model.TEM_Interface;
import java.util.List;

public interface TEMInterfaceService {
	
	TEM_Interface addTemInterface(TEM_Interface temInterface);

	void deleteTemInterface(long id);

	TEM_Interface getTemInterfaceById(long id);

	TEM_Interface editTemInterface(TEM_Interface temInterface);

	List<TEM_Interface> getAll();
}
