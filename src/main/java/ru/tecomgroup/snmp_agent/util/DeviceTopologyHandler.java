package ru.tecomgroup.snmp_agent.util;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TreeEvent;

import ru.tecomgroup.snmp_agent.ShareConstants;
import ru.tecomgroup.snmp_agent.model.TEM_Device;
import ru.tecomgroup.snmp_agent.model.TEM_Interface;
import ru.tecomgroup.snmp_agent.model.TEM_SnmpSettings;
import ru.tecomgroup.snmp_agent.model.TEM_System;
import ru.tecomgroup.snmp_agent.snmp.exception.SnmpRequestException;
import ru.tecomgroup.snmp_agent.snmp.exception.SnmpSessionException;

public class DeviceTopologyHandler {
	private static final Logger LOGGER = Logger.getLogger(DeviceTopologyHandler.class);

	public static final OID systemSectionOid = new OID("1.3.6.1.2.1.1");

	public static void syncTopology(TEM_Device device) {
		syncSystemSection(device);
		syncInterfaces(device);
	}

	private static boolean syncSystemSection(TEM_Device device) {
		List<TreeEvent> event = null;

		try {
			event = SnmpUtil.getSubtree(device, systemSectionOid);
		} catch (SnmpSessionException | SnmpRequestException e) {
			LOGGER.warn(e.getMessage());
		} 
		
		if (event == null) {
			LOGGER.warn("TreeEvent is null");
			return false;
		}
		if (event.size() == 0) {
			LOGGER.warn("List TreeEvent is empty");
			return false;
		}
		
		event.stream().forEach(treeEvent -> {
			TEM_System systemSection = device.getSystem();
			if(systemSection == null) {
				systemSection = new TEM_System();
				device.setSystem(systemSection);
			}
			String sysUpTime = SnmpUtil.getTimeTikcsValue(treeEvent, SnmpConstants.sysUpTime);
			systemSection.setSysUpTime(sysUpTime);

			String sysDescription = SnmpUtil.getStringValue(treeEvent, SnmpConstants.sysDescr);
			systemSection.setSysDescr(sysDescription);

			String sysContact = SnmpUtil.getStringValue(treeEvent, SnmpConstants.sysContact);
			systemSection.setSysContact(sysContact);

			String sysLocation = SnmpUtil.getStringValue(treeEvent, SnmpConstants.sysLocation);
			systemSection.setSysLocation(sysLocation);

			String sysObjectID = SnmpUtil.getOIDValue(treeEvent, SnmpConstants.sysObjectID);
			systemSection.setSysObjectID(sysObjectID);

			String sysName = SnmpUtil.getStringValue(treeEvent, SnmpConstants.sysName);
			systemSection.setSysName(sysName);

			String sysServices = SnmpUtil.getIntValue(treeEvent, SnmpConstants.sysServices).toString();
			systemSection.setSysServices(sysServices);
			
		});
		return true;
	}

	private static void syncInterfaces(TEM_Device device) {
		OID[] INTERFACE_TABLE_OID = { ShareConstants.IF_INDEX, ShareConstants.IF_TYPE, ShareConstants.IF_ADMIN_STATUS,
				ShareConstants.IF_DESCR, ShareConstants.IF_MTU, ShareConstants.IF_OPER_STATUS,
				ShareConstants.IF_PHYS_ADDRESS, ShareConstants.IF_SPEED };
		List<TableEvent> tableEvents = null;
		try {
			tableEvents = SnmpUtil.getTable(device, INTERFACE_TABLE_OID);
		} catch (SnmpSessionException e) {
			LOGGER.error(e.getMessage());
		}
		Set<Integer> newInterfaces = new HashSet<>();
		Map<Integer, TEM_Interface> interfaces = device.getInterfaces();

		if (tableEvents != null) {
			for (TableEvent event : tableEvents) {
				Integer ifIndex = SnmpUtil.getIntValue(event, ShareConstants.IF_INDEX);
				TEM_Interface intf = interfaces.get(ifIndex);
				if (intf == null) {
					intf = new TEM_Interface();
					intf.setIfIndex(ifIndex);
					interfaces.put(ifIndex, intf);
				}
				intf.setIfDescr(SnmpUtil.getOctetStringValue(event, ShareConstants.IF_DESCR));
				intf.setIfType(SnmpUtil.getIfType(event));
				intf.setIfMtu(SnmpUtil.getIntValue(event, ShareConstants.IF_MTU));
				intf.setIfSpeed(SnmpUtil.getGaugeValue(event, ShareConstants.IF_SPEED));
				intf.setIfPhysAddress(SnmpUtil.getOctetStringValue(event, ShareConstants.IF_PHYS_ADDRESS));
				intf.setIfOperStatus(SnmpUtil.getOperStatus(event));
				intf.setIfAdminStatus(SnmpUtil.getAdminStatus(event));
				intf.setName(device.getName());
				newInterfaces.add(intf.getIfIndex());

			}
			interfaces.keySet().retainAll(newInterfaces);
		}
	}

	public static TEM_SnmpSettings createSnmpSettings(String address, Integer requestPort, String readCommunity,
			String writeCommunity) {
		TEM_SnmpSettings settings = new TEM_SnmpSettings();
		settings.setAddress(address);
		settings.setReadCommunity(readCommunity);
		settings.setWriteCommunity(writeCommunity);
		settings.setRequestPort(requestPort);
		settings.setRetries(ShareConstants.DEFAULT_SNMP_RETRIES);
		settings.setTimeout(ShareConstants.DEFAULT_SNMP_TIMEOUT);
		settings.setSnmpVersion(ShareConstants.DEFAULT_SNMP_VERSION);
		return settings;
	}
}
