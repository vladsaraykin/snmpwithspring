package ru.tecomgroup.snmp_agent.util;

import org.apache.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.Snmp;
import org.snmp4j.smi.*;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.PDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

import ru.tecomgroup.snmp_agent.ShareConstants;
import ru.tecomgroup.snmp_agent.model.TEM_Device;
import ru.tecomgroup.snmp_agent.model.type_enum.IfAdminStatus;
import ru.tecomgroup.snmp_agent.model.type_enum.IfOperStatus;
import ru.tecomgroup.snmp_agent.model.type_enum.IfType;
import ru.tecomgroup.snmp_agent.snmp.SnmpSessionManager;
import ru.tecomgroup.snmp_agent.snmp.SnmpSessionManager.SnmpSessionType;
import ru.tecomgroup.snmp_agent.snmp.exception.SnmpRequestException;
import ru.tecomgroup.snmp_agent.snmp.exception.SnmpSessionException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class SnmpUtil {
	private static final Logger LOGGER = Logger.getLogger(SnmpUtil.class);

	public static List<TreeEvent> getSubtree(TEM_Device device, OID oid)
			throws SnmpSessionException, SnmpRequestException {
		if (oid == null) {
			throw new IllegalArgumentException("The argument 'oid' must not be null");
		}
		Snmp snmpSession = SnmpSessionManager.getInstance().getSession(SnmpSessionType.CLIENT);
		PDUFactory pduFactory = new DefaultPDUFactory();
		CommunityTarget target = createTarget(device);
		TreeUtils treeUtils = new TreeUtils(snmpSession, pduFactory);
		List<TreeEvent> event = treeUtils.getSubtree(target, oid);
		return event;
	}

	public static List<TableEvent> getTable(TEM_Device device, OID[] oids) throws SnmpSessionException {
		if (oids == null) {
			throw new IllegalArgumentException("The argument 'oid' must not be null");
		}

		Snmp snmpSession = SnmpSessionManager.getInstance().getSession(SnmpSessionType.CLIENT);
		TableUtils tUtils = new TableUtils(snmpSession, new DefaultPDUFactory());
		CommunityTarget target = createTarget(device);
		return tUtils.getTable(target, oids, null, null);
	}

	private static CommunityTarget createTarget(TEM_Device device) {
		CommunityTarget target = new CommunityTarget();
		String ip = device.getSetting().getAddress();
		String port = device.getSetting().getRequestPort().toString();
		String community = device.getSetting().getReadCommunity();
		target.setAddress(GenericAddress.parse(ip + "/" + port));
		target.setCommunity(new OctetString(community));
		target.setVersion(device.getSetting().getSnmpVersion());
		target.setRetries(device.getSetting().getRetries());
		target.setTimeout(device.getSetting().getTimeout());
		return target;
	}

	private static <T> T getValue(VariableBinding[] vbs, OID oid, Predicate<Variable> checkType,
			Function<Variable, T> getResult) {
		T result = null;
		for (VariableBinding vb : vbs) {
			if (vb != null && vb.getOid() != null) {
				if (vb.getOid().startsWith(oid)) {
					Variable v = vb.getVariable();
					if (v != null && checkType.test(v)) {
						result = getResult.apply(v);
					}
				}
			}
		}
		return result;
	}

	private static <T> T getValue(TableEvent tableEvent, OID oid, Predicate<Variable> checkType,
			Function<Variable, T> getResult) {
		VariableBinding[] vbs = tableEvent.getColumns();
		T result = getValue(vbs, oid, checkType, getResult);
		return result;
	}

	private static <T> T getValue(TreeEvent treeEvent, OID oid, Predicate<Variable> checkType,
			Function<Variable, T> getResult) {
		VariableBinding[] vbs = treeEvent.getVariableBindings();
		T result = getValue(vbs, oid, checkType, getResult);
		return result;
	}

	public static String getOctetStringValue(VariableBinding[] vbs, OID oid) {
		return getValue(vbs, oid, checkTypeString, getResultString);
	}

	public static String getStringValue(TreeEvent event, OID oid) {
		String result = getValue(event, oid, checkTypeString, getResultString);
		return result;
	}

	public static Integer getIntValue(TreeEvent event, OID oid) {
		Integer result = getValue(event, oid, checkTypeInt, getResultInt);
		return result;
	}

	public static String getTimeTikcsValue(TreeEvent event, OID oid) {
		String result = getValue(event, oid, checkTypeTimeTicks, getResultTimeTicks).toString();
		return result;
	}

	public static String getOIDValue(TreeEvent event, OID oid) {
		String result = getValue(event, oid, checkTypeOID, getResultOID);
		return result;
	}

	private static final Predicate<Variable> checkTypeTimeTicks = v -> v.getSyntax() == SMIConstants.SYNTAX_TIMETICKS;
	private static final Function<Variable, Long> getResultTimeTicks = v -> ((TimeTicks) v).getValue();

	private static final Predicate<Variable> checkTypeOID = v -> v.getSyntax() == SMIConstants.SYNTAX_OBJECT_IDENTIFIER;
	private static final Function<Variable, String> getResultOID = v -> v.toString();

	private static final Predicate<Variable> checkTypeInt = v -> v.getSyntax() == SMIConstants.SYNTAX_INTEGER32;
	private static final Function<Variable, Integer> getResultInt = v -> v.toInt();

	public static Integer getIntValue(TableEvent tableEvent, OID oid) {
		return getValue(tableEvent, oid, checkTypeInt, getResultInt);
	}

	private static final Predicate<Variable> checkTypeString = v -> v.getSyntax() == SMIConstants.SYNTAX_OCTET_STRING;
	private static final Function<Variable, String> getResultString = v -> convertOctetStringToString((OctetString) v,
			StandardCharsets.UTF_8);

	public static String getOctetStringValue(TableEvent tableEvent, OID oid) {
		return getValue(tableEvent, oid, checkTypeString, getResultString);
	}

	private static final Predicate<Variable> checkTypeGauge = v -> v.getSyntax() == SMIConstants.SYNTAX_GAUGE32;
	private static final Function<Variable, Integer> getResultGauge = v -> v.toInt();

	public static Integer getGaugeValue(TableEvent tableEvent, OID oid) {
		return getValue(tableEvent, oid, checkTypeGauge, getResultGauge);
	}

	public static final IfAdminStatus getAdminStatus(TableEvent tableEvent) {
		Integer resultStatus = getIntValue(tableEvent, ShareConstants.IF_ADMIN_STATUS);
		return IfAdminStatus.getIfAdminStatusById(resultStatus);
	}

	public static final IfOperStatus getOperStatus(TableEvent tableEvent) {
		Integer resultStatus = getIntValue(tableEvent, ShareConstants.IF_OPER_STATUS);
		return IfOperStatus.getIfOperStatus(resultStatus);
	}

	public static final IfType getIfType(TableEvent tableEvent) {
		Integer resultStatus = getIntValue(tableEvent, ShareConstants.IF_TYPE);
		return IfType.getIfType(resultStatus);
	}

	private static String convertOctetStringToString(OctetString octetString, Charset encoding) {
		String result = null;
		if (octetString != null) {
			if (octetString.isPrintable()) {
				result = new String(octetString.getValue(), encoding);
			} else {
				result = convertFromHexToString(octetString.toHexString(), encoding);
			}
		}
		return result;
	}

	private static String convertFromHexToString(String hexString, Charset encoding) {
		String result = null;
		if (hexString != null && HEX_STRING_PATTERN.matcher(hexString).matches()) {
			result = new String(OctetString.fromHexString(hexString).toByteArray(), encoding);
		} else {
			result = hexString;
		}

		// removes NUL chars
		if (result != null) {
			result = result.replaceAll("\u0000", "");
		}

		return result;
	}

	private static final Pattern HEX_STRING_PATTERN = Pattern.compile("^(?:[A-Fa-f0-9]{2}:)*(?:[A-Fa-f0-9]{2})?$");

}
