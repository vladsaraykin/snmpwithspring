package ru.tecomgroup.snmp_agent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ru.tecomgroup.snmp_agent.model.TEM_Device;
import ru.tecomgroup.snmp_agent.model.TEM_SnmpSettings;
import ru.tecomgroup.snmp_agent.service.impl.DeviceServiceImpl;
import ru.tecomgroup.snmp_agent.snmp.SnmpSessionManager;
import ru.tecomgroup.snmp_agent.util.DeviceTopologyHandler;

public class App {
	private static final Logger LOGGER = Logger.getLogger("snmpAgent");
	private static ApplicationContext ctx = new AnnotationConfigApplicationContext(PersistenceConfig.class);
	private static DeviceServiceImpl deviceService = ctx.getBean(DeviceServiceImpl.class);
    private static Map<Long, String> deviceMap = new HashMap<>();
    private static ConsoleScanner scanner = new ConsoleScanner();

	public static void main(String[] args) {
	    deviceService.getAll().forEach(device -> {
	        deviceMap.put(device.getId(), device.getName());
        });
		boolean statusProgramm = true;
		do {
			System.out.println();
			printMenu();
			System.out.println();
			Integer menuCommandSelected = scanner.getIntegerValue("Please select menu item: ");
			try {
				switch (menuCommandSelected) {
				case 1:
					addClient(scanner);
					break;
				case 2:
					removeClient(scanner);
					break;
				case 3:
					showClients();
					break;
				case 4:
					runClientCommands(scanner);
					break;
				case 5:
					SnmpSessionManager.getInstance().stopAllSnmpSession();
					statusProgramm = false;
					break;
				default:
					LOGGER.info("For '" + menuCommandSelected + "' there is no command in the menu. Please enter a value from the displayed menu");
					break;
				}
			} catch (Exception e) {
				LOGGER.error("Failed to execute the command " + menuCommandSelected, e);
			}

		} while (statusProgramm);

		System.exit(0);

	}

	private static void runClientCommands(ConsoleScanner scanner) {
		if(showClients()) {
			System.out.println();
			Integer clientId = scanner.getIntegerValue("Select client (enter id): ");
			TEM_Device device = deviceService.getDeviceById(clientId.longValue());
			boolean statusCommands = true;
			do {
				System.out.print("Commands (1 - Sync topology, 2 - Exit): ");
				Integer clientCommandSelected = scanner.getIntegerValue();
				switch (clientCommandSelected) {
				case 1:
					try {
						DeviceTopologyHandler.syncTopology(device);
						device = deviceService.editDevice(device);
					} catch (Exception e) {
						LOGGER.warn("Exception", e);
					}
					
					break;
				case 2:
					statusCommands = false;
				default:
                    LOGGER.info("For '" + clientCommandSelected + "' there is no command in the menu. Please enter a value from the displayed menu");
					break;
				}
			} while (statusCommands);
		}
	}

	private static boolean showClients() {
		if (deviceMap != null && deviceMap.size() != 0) {
           deviceMap.forEach((k, v) -> {
               System.out.println("ID: " + k + " name device: " + v);
           });
			return true;
		}
		LOGGER.info("Devices list is empty");
		return false;
	}

	private static void removeClient(ConsoleScanner scanner) {
		if(showClients()) {
			System.out.println();
			Integer clientId = scanner.getIntegerValue("Select client (enter id): ");
			try {
				deviceService.deleteDeviceById(clientId.longValue());
				deviceMap.remove(clientId.longValue());
			} catch (Exception e) {
				LOGGER.error("Failed to remove client with id= '" + clientId + "'", e);
			}
		}
	}

	private static void addClient(ConsoleScanner scanner) {
		LOGGER.info("Adding client...");
		System.out.println("Please fill in the following required fields");
		String clientName = scanner.getStringValue("Name: ");
		String address = scanner.getStringValue("Ip address: ");
		Integer requestPort = scanner.getIntegerValue("Port: ", ShareConstants.DEFAULT_PORT);
		String readCommunity = scanner.getStringValue("Read community: ", ShareConstants.DEFAULT_READ_COMMUNITY);
		String writeCommunity = scanner.getStringValue("Write community: ", ShareConstants.DEFAULT_WRITE_COMMUNITY);
		System.out.println();

		TEM_Device device = new TEM_Device();
		TEM_SnmpSettings settings = new TEM_SnmpSettings();
		device.setName(clientName);
		settings.setAddress(address);
		settings.setRequestPort(requestPort);
		settings.setReadCommunity(readCommunity);
		settings.setWriteCommunity(writeCommunity);
		settings.setRetries(ShareConstants.DEFAULT_SNMP_RETRIES);
		settings.setTimeout(ShareConstants.DEFAULT_SNMP_TIMEOUT);
		settings.setSnmpVersion(ShareConstants.DEFAULT_SNMP_VERSION);
		device.setSetting(settings);
		TEM_Device updateDevice = deviceService.addDevice(device);
		deviceMap.put(updateDevice.getId(), updateDevice.getName());
		LOGGER.info("Client was added");
	}

	private static void printMenu() {
		System.out.println("#####################################");
		System.out.println("#  1 - Add client                   #");
		System.out.println("#  2 - Remove client                #");
		System.out.println("#  3 - View clients                 #");
		System.out.println("#  4 - Run command for client       #");
		System.out.println("#  5 - Exit                         #");
		System.out.println("#####################################");
	}

}
