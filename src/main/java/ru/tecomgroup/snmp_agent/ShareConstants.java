package ru.tecomgroup.snmp_agent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;

public class ShareConstants {

	public static final int DEFAULT_SNMP_RETRIES = 3;
	public static final long DEFAULT_SNMP_TIMEOUT = 1000L;
	public static final int DEFAULT_SNMP_VERSION = SnmpConstants.version2c;
	public final static OID IF_INDEX = new OID("1.3.6.1.2.1.2.2.1.1");
	public final static OID IF_DESCR = new OID("1.3.6.1.2.1.2.2.1.2");
	public final static OID IF_TYPE = new OID("1.3.6.1.2.1.2.2.1.3");
	public final static OID IF_MTU = new OID("1.3.6.1.2.1.2.2.1.4");
	public final static OID IF_SPEED = new OID("1.3.6.1.2.1.2.2.1.5");
	public final static OID IF_PHYS_ADDRESS = new OID("1.3.6.1.2.1.2.2.1.6");
	public final static OID IF_ADMIN_STATUS = new OID("1.3.6.1.2.1.2.2.1.7");
	public final static OID IF_OPER_STATUS = new OID("1.3.6.1.2.1.2.2.1.8");
	public static final Integer DEFAULT_PORT = 161;
	public static final String DEFAULT_READ_COMMUNITY = "public";
	public static final String DEFAULT_WRITE_COMMUNITY = "private";
}
